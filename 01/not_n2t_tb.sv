module not_n2t_tb();
    reg in;
    reg expected;
    reg out;

    not_n2t gate1(in, out);

    initial begin
        in = 0;
        #1 assert (out == ~in);
        #1 in = 1;
        #1 assert (out == ~in);
    end
endmodule
